# Higher Order Function 

The function that takes another function as arguments or return other functions as their output is known as a higher-order function.

Taking another function as an argument is also called a callback function because it is call back by a higher-order function.This concept uses a lot in javascript.

Let's look at example for better understanding.

**Function as an argument to another function**
 ```
const addition = function (value1, value2) {
  return value1 + value2;
};
const multiplication = function (value1, value2) {
  return value1 * value2;
};

const data = function (func) {
  let number1 = 4;
  let number2 = 6;
  return func(number1, number2);
};

console.log(data(addition)); // 10
console.log(data(multiplication)); // 24
```
In the above program, data() is a higher-order function that takes the function func() as an argument and manipulates the elements of an array according to a given operation. Here we generate two functions addition and multiplication taking as an argument to data function and generate an output of element addition is 10. Similarly, for multiplication output generate 24.


**Functions that return other functions**
```
function divison(number1) {
  return function (number2) {
    return number1 / number2;
  };
}

console.log(divison(20)(2)); // 10
```
In the above program, the function returns another function to perform division in two numbers. It gives an output is 10.


## Built-in higher-order functions in JavaScript

Many of the built-in JavaScript functions, such as arrays, strings, DOM methods, and promise methods, are higher-order functions that provide a significant level of abstraction.

Below are some of the built-in higher-order functions:

* Array.prototype.map()
* Array.prototype.filter()
* Array.prototype.reduce()
* Array.prototype.forEach()
* Array.prototype.sort()


### Array.prototype.map()

The map method takes every array element to creates a new array from callback function provided as an arguments. For empty elements it does not execute the function. The original array will not change.

**Syntax**

The map method is represented by the following syntax:
```
array.map(callback(currentValue, index, array), thisValue) 

(or)

array.map((currentValue) => {
    // write code here
}) 
```

**Parameters**

* callback() -> It is required. To run for each array element.
* currentValue -> It is required. It gives current element value.
* index -> It is optional. Current element index.
* array -> It is optional. Store the original array.
* thisValue -> It is optional. Set the default value is undefined.

**Return value**

It return a new array with the same length as the original array.

Let's look at example:
```
const flower = ["rose", "jasmine", "lily", "orchid", "mariagold"];
const upperCaseFlower = flower.map((element) => {
  return element.toUpperCase();
});

console.log(upperCaseFlower); // [ 'ROSE', 'JASMINE', 'LILY', 'ORCHID', 'MARIAGOLD' ]
```
The above example explains, to create an array of string names is the flower. Here we convert the array element into an upper-case string by using the map method. The map will iterate all elements in an array and convert them into uppercase. The output will store in the new array which is taken here upperCaseFlower. The original array length and return array length are the same but each element is modified. 


### Array.prototype.filter()

The filter method passes all elements from test provided by the callback function and creates a new array from the output of callback function. For empty elements it does not execute the function. The original array will not change.

**Syntax**

The filter method is represented by the following syntax:
```
array.filter(callback(currentValue, index, array), thisValue)  

(or)

array.filter((currentValue) => {
    // write code here
})
```

**Parameters**

* callback() -> It is required. To run for each array element.
* currentValue -> It is required. It gives current element value.
* index -> It is optional. Current element index.
* array -> It is optional. Store the original array.
* thisValue -> It is optional. Set the default value is undefined.

**Return value**

It returns a new array which contains the elements that pass in certain conditions. The length will be depend on condition passed. If no elements pass the condition it returns an empty array.

Let's look at example:
```
const number = [2, 5, 7, 4, 9, 10];
const oddNumber = number.filter((element) => {
  return element % 2 !== 0;
});

console.log(oddNumber); // [ 5, 7, 9 ]
```
The above example explained, the input is taken as an array of numbers and filters out the odd number in an array. To filter the array we use the filter method to abstract only odd numbers from the given input and create new array output to save the output result in oddNumber.The output array length is changed due to the specific operation we performed here.


### Array.prototype.reduce()

The reduce method runs the callback function on each elements of the calling array which results in a single output value. The reduce method accepts two parameters are reducer or callback function  and  an optional initialValue. For empty elements it does not execute the function. The original array will not change.

**Syntax**

The reduce method is represented by the following syntax:
```
array.reduce(callback(accumulator, currentValue, index, array), initialValue)

(or)

array.reduce((accumulator, currentValue) => {
    // write code here 
},initialValue)
```

**Parameters**

* callback() -> It is required. To run for each array element.
* accumulator -> It is required. InitialValue OR previously returned value.
* currentValue -> It is required. It gives current element value.
* index -> It is optional. Current element index.
* array -> It is optional. Store the original array.
* initialValue -> It is optional. To specify the value to be passed to the function as the initial value.

**Return value**

It returns a single value array. The last call of the callback function gives its single value output.

Let's look at example:
```
const number = [1, 2, 3, 4, 5];
const multiplyAllNumber = number.reduce((accumulator, element) => {
  return accumulator * element;
});

console.log(multiplyAllNumber); // 120
```
The above example explained, we create an input here array of numbers and create a variable name multiplyAllNumber to store the output. Here we want to multiply all element which is present in the input. That's why we used reduce method to get the single value of all multiple elements. The accumulator stores the previous value of the result and multiplies it with the current element and gives output.


### Array.prototype.forEach()

The forEach method calls a function for each element in an array. For empty elements it does not execute the function.

**Syntax**

The forEach method is represented by the following syntax:
```
array.forEach(callback(currentValue, index, array), thisValue) 

(or)

array.forEach((currentValue) => {
    // write code here
}) 
```

**Parameters**

* callback() -> It is required. To run for each array element.
* currentValue -> It is required. It gives current element value.
* index -> It is optional. Current element index.
* array -> It is optional. Store the original array.
* thisValue -> It is optional. Set the default value is undefined.

**Return value**

It returns undefined. It is not chain with other methods.

Let's look at example:
```
const number = [1, 2, 3, 4, 5];
const addNumber2 = [];
number.forEach((element) => {
  return addNumber2.push(element + 2);
});

console.log(addNumber2); // [ 3, 4, 5, 6, 7 ]
```
The above example explained, we want all elements will iterates with an increased value by 2. So we used forEach to iterate all element line by line. And push the output in addNumber2 to store the result. If we directly do the operation it gives an undefined return value. It is not chaining with other methods.


### Array.prototype.sort()

The sort method sorts the elements of an array, overwrites the original array. It is sorts the elements as strings in alphabetical and ascending order.

**Syntax**
```
array.sort()

(or)

array.sort(compareFunction)

(or)

array.sort((valueA, valueB) => {
    // write code here
})
```

**Parameters**

* compareFunction -> It is optional. Its defined a sort order. The value of function return a positive, negative, or zero depends upon the arguments.
* valueA -> The first element for comparison.
* valueB -> The second element for comparison.

**Return value**

It returns the reference of sorted original array. It can sorting order be ascending or descending depending on the condition given. By default, the sort is in ascending order

Let's look at example:
```
const value = [40, 125, 1, 5, 25, 150];
const ascendingValue = value.sort((valueA, valueB) => {
  return valueA - valueB;
});

console.log(ascendingValue); // [ 1, 5, 25, 40, 125, 150 ]
```
The above example explained, input is taken as an array of numbers and performs a sorting operation in ascending wise. we want the output number in the form of ascending order that's why we subtract the first parameter valueA from the second one valueB. The output array length and input array length are the same because we iterate all elements in an array and store the output in the new array.


## Conclusion

In JavaScript, higher-order functions are a beneficial feature. It enhances the functionality of ordinary functions, reusability increases, and abstraction of code.

We can either create our own to write composed and complex functions according to our requirements or use the built-in higher-order functions present in JavaScript libraries.












